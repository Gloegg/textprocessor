unit uFormRuleNCD;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uTextProcessor, Vcl.ComCtrls,
  Vcl.StdCtrls, Vcl.Samples.Spin, Vcl.ExtCtrls;

type
  TfrmRuleNCD = class(TForm)
    lbType: TListBox;
    pcType: TPageControl;
    pgInsert: TTabSheet;
    pgDelete: TTabSheet;
    pgRemove: TTabSheet;
    pgReplace: TTabSheet;
    pgRearrange: TTabSheet;
    pgStrip: TTabSheet;
    pgCase: TTabSheet;
    pgSerialize: TTabSheet;
    pgCleanUp: TTabSheet;
    btOK: TButton;
    btCancel: TButton;
    edInsertText: TEdit;
    rgInsertPosition: TRadioGroup;
    edInsertPosition: TSpinEdit;
    edInsertAfter: TEdit;
    edInsertBefore: TEdit;
    Label1: TLabel;
    rgDeleteFrom: TRadioGroup;
    rgDeleteUntil: TRadioGroup;
    cbDeleteDelim: TCheckBox;
    edDeleteFromPosition: TSpinEdit;
    edDeleteUntilCount: TSpinEdit;
    edDeleteUntilText: TEdit;
    edDeleteFromText: TEdit;
    cbCleanUpBraces: TCheckBox;
    cbCleanUpSquare: TCheckBox;
    cbCleanUpCurly: TCheckBox;
    cbCleanUpDots: TCheckBox;
    cbCleanUpFixSpaces: TCheckBox;
    cbCleanUpComma: TCheckBox;
    cbCleanUpPlus: TCheckBox;
    cbCleanUpMinus: TCheckBox;
    cbCleanUpUnderscore: TCheckBox;
    edRemoveText: TEdit;
    rgRemoveOccurrence: TRadioGroup;
    cbRemoveCaseSensitive: TCheckBox;
    cbRemoveWildcards: TCheckBox;
    edReplaceSearch: TEdit;
    edReplaceText: TEdit;
    rgReplaceOccurrences: TRadioGroup;
    cbReplaceCaseSensitive: TCheckBox;
    cbReplaceWildcards: TCheckBox;
    Label2: TLabel;
    Label3: TLabel;
    rgRearrangeMode: TRadioGroup;
    edRearrangeSplitter: TEdit;
    edRearrangePattern: TEdit;
    Label5: TLabel;
    cbStripInvert: TCheckBox;
    cbStripLetters: TCheckBox;
    edStripLetters: TEdit;
    cbStripDigits: TCheckBox;
    edStripDigits: TEdit;
    cbStripSymbols: TCheckBox;
    edStripSymbols: TEdit;
    cbStripBrackets: TCheckBox;
    edStripBrackets: TEdit;
    cbStripUser: TCheckBox;
    edStripUser: TEdit;
    edSerializeStart: TSpinEdit;
    edSerializeStep: TSpinEdit;
    cbSerializePad: TCheckBox;
    edSerializePad: TSpinEdit;
    Label4: TLabel;
    Label6: TLabel;
    rgSerializeMode: TRadioGroup;
    edSerializePosition: TSpinEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    rgCaseMode: TRadioGroup;
    edCaseIgnore: TEdit;
    Label11: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure lbTypeClick(Sender: TObject);
    procedure btOKClick(Sender: TObject);
    procedure rgInsertPositionClick(Sender: TObject);
    procedure rgDeleteFromClick(Sender: TObject);
  private
    FTextProcessor: ITextProcessor;
    procedure CreateInsert;
    procedure CreateDelete;
    procedure CreateRemove;
    procedure CreateReplace;
    procedure CreateRearrange;
    procedure CreateStrip;
    procedure CreateCase;
    procedure CreateSerialize;
    procedure CreateCleanUp;

    procedure ShowInsert;
    procedure ShowDelete;
    procedure ShowRemove;
    procedure ShowReplace;
    procedure ShowRearrange;
    procedure ShowStrip;
    procedure ShowCase;
    procedure ShowSerialize;
    procedure ShowCleanUp;
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    property TextProcessor: ITextProcessor read FTextProcessor
      write FTextProcessor;
    procedure SetTextProcessor(aTextProcessor: ITextProcessor);
  end;

var
  frmRuleNCD: TfrmRuleNCD;

implementation

{$R *.dfm}

uses
  Generics.Collections;

procedure TfrmRuleNCD.btOKClick(Sender: TObject);
begin
  case lbType.ItemIndex of
    0:
      CreateInsert;
    1:
      CreateDelete;
    2:
      CreateRemove;
    3:
      CreateReplace;
    4:
      CreateRearrange;
    5:
      CreateStrip;
    6:
      CreateCase;
    7:
      CreateSerialize;
    8:
      CreateCleanUp;
  end;
end;

procedure TfrmRuleNCD.CreateCase;
var
  tp: TCase;
begin
  tp := TCase.Create;
  FTextProcessor := tp;
  case rgCaseMode.ItemIndex of
    0:
      tp.Mode := cmEveryWord;
    1:
      tp.Mode := cmPreserve;
    2:
      tp.Mode := cmLower;
    3:
      tp.Mode := cmUpper;
    4:
      tp.Mode := cmInvert;
    5:
      tp.Mode := cmFirstLetter;
  end;
  tp.IgnoreText := edCaseIgnore.Text;
end;

procedure TfrmRuleNCD.CreateCleanUp;
var
  tp: TCleanUp;
begin
  tp := TCleanUp.Create;
  FTextProcessor := tp;
  tp.StripBraces := cbCleanUpBraces.Checked;
  tp.StripSquareBrackets := cbCleanUpSquare.Checked;
  tp.StripCurlyBraces := cbCleanUpCurly.Checked;
  tp.ReplaceDots := cbCleanUpDots.Checked;
  tp.ReplaceComma := cbCleanUpComma.Checked;
  tp.ReplaceUnderscore := cbCleanUpUnderscore.Checked;
  tp.ReplacePlus := cbCleanUpPlus.Checked;
  tp.ReplaceMinus := cbCleanUpMinus.Checked;
  tp.FixSpaces := cbCleanUpFixSpaces.Checked;
end;

procedure TfrmRuleNCD.CreateDelete;
var
  tp: TDelete;
begin
  tp := TDelete.Create;
  FTextProcessor := tp;
  case rgDeleteFrom.ItemIndex of
    0:
      tp.From.Option := dfoPosition;
    1:
      tp.From.Option := dfoDelimiter;
  end;

  tp.From.Position := edDeleteFromPosition.Value;
  tp.From.Delimiter := edDeleteFromText.Text;
  case rgDeleteUntil.ItemIndex of
    0:
      tp.Till.Option := duoCount;
    1:
      tp.Till.Option := duoDelimiter;
    2:
      tp.Till.Option := duoTillTheEnd;
  end;
  tp.Till.Count := edDeleteUntilCount.Value;
  tp.Till.Delimiter := edDeleteUntilText.Text;
  tp.DeleteDelimiters := cbDeleteDelim.Checked;
end;

procedure TfrmRuleNCD.CreateInsert;
var
  tp: TInsert;
begin
  tp := TInsert.Create;
  FTextProcessor := tp;
  tp.Text := edInsertText.Text;
  case rgInsertPosition.ItemIndex of
    0:
      tp.Position := posPrefix;
    1:
      tp.Position := posSuffix;
    2:
      tp.Position := posExact;
    3:
      begin
        tp.Position := posAfter;
        tp.PositionText := edInsertAfter.Text;
      end;
    4:
      begin
        tp.Position := posBefore;
        tp.PositionText := edInsertBefore.Text;
      end;
  end;
  tp.PositionIndex := edInsertPosition.Value;
end;

procedure TfrmRuleNCD.CreateRearrange;
var
  tp: TRearrange;
begin
  tp := TRearrange.Create;
  FTextProcessor := tp;
  tp.Splitters := edRearrangeSplitter.Text;
  tp.Pattern := edRearrangePattern.Text;
  case rgRearrangeMode.ItemIndex of
    0:
      tp.Option := raoDelimiters;
    1:
      tp.Option := raoPositions;
  end;
end;

procedure TfrmRuleNCD.CreateRemove;
var
  tp: TRemove;
begin
  tp := TRemove.Create;
  FTextProcessor := tp;
  tp.Text := edRemoveText.Text;
  case rgRemoveOccurrence.ItemIndex of
    0:
      tp.Occurrences := occAll;
    1:
      tp.Occurrences := occFirst;
    2:
      tp.Occurrences := occLast;
  end;
  tp.CaseSensitive := cbRemoveCaseSensitive.Checked;
  tp.UseWildcards := cbRemoveWildcards.Checked;
end;

procedure TfrmRuleNCD.CreateReplace;
var
  tp: TReplace;
begin
  tp := TReplace.Create;
  FTextProcessor := tp;
  tp.SearchText := edReplaceSearch.Text;
  tp.ReplaceText := edReplaceText.Text;
  case rgReplaceOccurrences.ItemIndex of
    0:
      tp.Occurrences := occAll;
    1:
      tp.Occurrences := occFirst;
    2:
      tp.Occurrences := occLast;
  end;
  tp.CaseSensitive := cbReplaceCaseSensitive.Checked;
  tp.UseWildcards := cbReplaceWildcards.Checked;
end;

procedure TfrmRuleNCD.CreateSerialize;
var
  tp: TSerialize;
begin
  tp := TSerialize.Create;
  FTextProcessor := tp;
  tp.IndexStart := edSerializeStart.Value;
  tp.IndexStep := edSerializeStep.Value;
  tp.Pad := cbSerializePad.Checked;
  tp.PadLength := edSerializePad.Value;
  case rgSerializeMode.ItemIndex of
    0:
      tp.Position := spPrefix;
    1:
      tp.Position := spSuffix;
    2:
      tp.Position := spExact;
  end;
  tp.PositionIndex := edSerializePosition.Value;
end;

procedure TfrmRuleNCD.CreateStrip;
var
  tp: TStrip;
begin
  tp := TStrip.Create;
  FTextProcessor := tp;
  tp.StripLetters := cbStripLetters.Checked;
  tp.StripDigits := cbStripDigits.Checked;
  tp.StripSymbols := cbStripSymbols.Checked;
  tp.StripBraces := cbStripBrackets.Checked;
  tp.StripUser := cbStripUser.Checked;
  tp.Chars := edStripUser.Text;
  tp.Invert := cbStripInvert.Checked;
end;

procedure TfrmRuleNCD.FormCreate(Sender: TObject);
begin
  lbType.ItemIndex := 0;
  pcType.ActivePageIndex := 0;
end;

procedure TfrmRuleNCD.lbTypeClick(Sender: TObject);
begin
  pcType.ActivePageIndex := lbType.ItemIndex;
end;

procedure TfrmRuleNCD.rgDeleteFromClick(Sender: TObject);
begin
  edDeleteFromPosition.Enabled := rgDeleteFrom.ItemIndex = 0;
  edDeleteFromText.Enabled := rgDeleteFrom.ItemIndex = 1;
  edDeleteUntilCount.Enabled := rgDeleteUntil.ItemIndex = 0;
  edDeleteUntilText.Enabled := rgDeleteUntil.ItemIndex = 1;
end;

procedure TfrmRuleNCD.rgInsertPositionClick(Sender: TObject);
begin
  edInsertPosition.Enabled := rgInsertPosition.ItemIndex = 2;
  edInsertAfter.Enabled := rgInsertPosition.ItemIndex = 3;
  edInsertBefore.Enabled := rgInsertPosition.ItemIndex = 4;
end;

procedure TfrmRuleNCD.SetTextProcessor(aTextProcessor: ITextProcessor);
var
  pages: TDictionary<string, integer>;

begin
  FTextProcessor := aTextProcessor;
  pages := TDictionary<string, integer>.Create;
  pages.Add('TInsert', 0);
  pages.Add('TDelete', 1);
  pages.Add('TRemove', 2);
  pages.Add('TReplace', 3);
  pages.Add('TRearrange', 4);
  pages.Add('TStrip', 5);
  pages.Add('TCase', 6);
  pages.Add('TSerialize', 7);
  pages.Add('TCleanUp', 8);
  pcType.ActivePageIndex := pages[aTextProcessor.getClassName];
  lbType.ItemIndex := pages[aTextProcessor.getClassName];
  pages.Free;
  if aTextProcessor is TInsert then
    ShowInsert
  else if aTextProcessor is TDelete then
    ShowDelete
  else if aTextProcessor is TRemove then
    ShowRemove
  else if aTextProcessor is TReplace then
    ShowReplace
  else if aTextProcessor is TRearrange then
    ShowRearrange
  else if aTextProcessor is TStrip then
    ShowStrip
  else if aTextProcessor is TCase then
    ShowCase
  else if aTextProcessor is TSerialize then
    ShowSerialize
  else if aTextProcessor is TCleanUp then
    ShowCleanUp;
end;

procedure TfrmRuleNCD.ShowCase;
var
  tp: TCase;
begin
  tp := FTextProcessor as TCase;
  case tp.Mode of
    cmEveryWord:
      rgCaseMode.ItemIndex := 0;
    cmPreserve:
      rgCaseMode.ItemIndex := 1;
    cmLower:
      rgCaseMode.ItemIndex := 2;
    cmUpper:
      rgCaseMode.ItemIndex := 3;
    cmInvert:
      rgCaseMode.ItemIndex := 4;
    cmFirstLetter:
      rgCaseMode.ItemIndex := 5;
  end;
  edCaseIgnore.Text := tp.IgnoreText;
end;

procedure TfrmRuleNCD.ShowCleanUp;
var
  tp: TCleanUp;
begin
  tp := FTextProcessor as TCleanUp;
  cbCleanUpBraces.Checked := tp.StripBraces;
  cbCleanUpSquare.Checked := tp.StripSquareBrackets;
  cbCleanUpCurly.Checked := tp.StripCurlyBraces;
  cbCleanUpDots.Checked := tp.ReplaceDots;
  cbCleanUpComma.Checked := tp.ReplaceComma;
  cbCleanUpPlus.Checked := tp.ReplacePlus;
  cbCleanUpMinus.Checked := tp.ReplaceMinus;
  cbCleanUpUnderscore.Checked := tp.ReplaceUnderscore;
  cbCleanUpFixSpaces.Checked := tp.FixSpaces;
end;

procedure TfrmRuleNCD.ShowDelete;
var
  tp: TDelete;
begin
  tp := FTextProcessor as TDelete;
  case tp.From.Option of
    dfoPosition:
      begin
        rgDeleteFrom.ItemIndex := 0;
        edDeleteFromPosition.Value := tp.From.Position;
      end;
    dfoDelimiter:
      begin
        rgDeleteFrom.ItemIndex := 1;
        edDeleteFromText.Text := tp.From.Delimiter;
      end;
  end;
  case tp.Till.Option of
    duoCount:
      begin
        rgDeleteUntil.ItemIndex := 0;
        edDeleteUntilCount.Value := tp.Till.Count;
      end;
    duoDelimiter:
      begin
        rgDeleteUntil.ItemIndex := 1;
        edDeleteUntilText.Text := tp.Till.Delimiter;
      end;
    duoTillTheEnd:
      begin
        rgDeleteUntil.ItemIndex := 2;
      end;
  end;
  cbDeleteDelim.Checked := tp.DeleteDelimiters;
end;

procedure TfrmRuleNCD.ShowInsert;
var
  tp: TInsert;
begin
  tp := FTextProcessor as TInsert;
  edInsertText.Text := tp.Text;
  case tp.Position of
    posPrefix:
      rgInsertPosition.ItemIndex := 0;
    posSuffix:
      rgInsertPosition.ItemIndex := 1;
    posExact:
      begin
        rgInsertPosition.ItemIndex := 2;
        edInsertPosition.Value := tp.PositionIndex;
      end;
    posAfter:
      begin
        rgInsertPosition.ItemIndex := 3;
        edInsertAfter.Text := tp.PositionText;
      end;
    posBefore:
      begin
        rgInsertPosition.ItemIndex := 4;
        edInsertBefore.Text := tp.PositionText;
      end;
  end;
end;

procedure TfrmRuleNCD.ShowRearrange;
var
  tp: TRearrange;
begin
  tp := FTextProcessor as TRearrange;
  edRearrangeSplitter.Text := tp.Splitters;
  edRearrangePattern.Text := tp.Pattern;
  case tp.Option of
    raoDelimiters:
      rgRearrangeMode.ItemIndex := 0;
    raoPositions:
      rgRearrangeMode.ItemIndex := 1;
  end;
end;

procedure TfrmRuleNCD.ShowRemove;
var
  tp: TRemove;
begin
  tp := FTextProcessor as TRemove;
  edRemoveText.Text := tp.Text;
  cbRemoveCaseSensitive.Checked := tp.CaseSensitive;
  cbRemoveWildcards.Checked := tp.UseWildcards;
  case tp.Occurrences of
    occAll:
      rgRemoveOccurrence.ItemIndex := 0;
    occFirst:
      rgRemoveOccurrence.ItemIndex := 1;
    occLast:
      rgRemoveOccurrence.ItemIndex := 2;
  end;
end;

procedure TfrmRuleNCD.ShowReplace;
var
  tp: TReplace;
begin
  tp := FTextProcessor as TReplace;
  edReplaceSearch.Text := tp.SearchText;
  edReplaceText.Text := tp.ReplaceText;
  cbReplaceCaseSensitive.Checked := tp.CaseSensitive;
  cbReplaceWildcards.Checked := tp.UseWildcards;
  case tp.Occurrences of
    occAll:
      rgReplaceOccurrences.ItemIndex := 0;
    occFirst:
      rgReplaceOccurrences.ItemIndex := 1;
    occLast:
      rgReplaceOccurrences.ItemIndex := 2;
  end;
end;

procedure TfrmRuleNCD.ShowSerialize;
var
  tp: TSerialize;
begin
  tp := FTextProcessor as TSerialize;
  edSerializeStart.Value := tp.IndexStart;
  edSerializeStep.Value := tp.IndexStep;
  cbSerializePad.Checked := tp.Pad;
  edSerializePad.Value := tp.PadLength;
  case tp.Position of
    spPrefix:
      rgSerializeMode.ItemIndex := 0;
    spSuffix:
      rgSerializeMode.ItemIndex := 1;
    spExact:
      begin
        rgSerializeMode.ItemIndex := 2;
        edSerializePosition.Value := tp.PositionIndex;
      end;
  end;
end;

procedure TfrmRuleNCD.ShowStrip;
var
  tp: TStrip;
begin
  tp := FTextProcessor as TStrip;
  cbStripLetters.Checked := tp.StripLetters;
  cbStripDigits.Checked := tp.StripDigits;
  cbStripSymbols.Checked := tp.StripSymbols;
  cbStripBrackets.Checked := tp.StripBraces;
  cbStripUser.Checked := tp.StripUser;
  edStripUser.Text := tp.Chars;
  cbStripInvert.Checked := tp.Invert;
end;

end.
