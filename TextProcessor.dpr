program TextProcessor;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  uMain in 'uMain.pas' {frmMain},
  uTextProcessor in 'uTextProcessor.pas',
  uFormRuleNCD in 'uFormRuleNCD.pas' {frmRuleNCD};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
