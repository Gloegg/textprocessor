unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Generics.Collections,
  uTextProcessor,
  Vcl.CheckLst, Vcl.ExtCtrls, Vcl.ImgList, cxGraphics;

type
  TfrmMain = class(TForm)
    btStart: TButton;
    btAddRule: TButton;
    GroupBox1: TGroupBox;
    lbRules: TCheckListBox;
    Splitter1: TSplitter;
    meOutput: TMemo;
    GroupBox2: TGroupBox;
    meInput: TMemo;
    Panel1: TPanel;
    Splitter2: TSplitter;
    ilSmall: TcxImageList;
    btOpen: TButton;
    btSave: TButton;
    odFile: TOpenDialog;
    sdFile: TSaveDialog;
    procedure btStartClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btAddRuleClick(Sender: TObject);
    procedure lbRulesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lbRulesClickCheck(Sender: TObject);
    procedure lbRulesDblClick(Sender: TObject);
    procedure lbRulesClick(Sender: TObject);
    procedure lbRulesDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure lbRulesDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure lbRulesStartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure meInputKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btOpenClick(Sender: TObject);
    procedure btSaveClick(Sender: TObject);
    procedure meInputChange(Sender: TObject);
  private
    { Private-Deklarationen }
    fRules: TList<ITextProcessor>;
    fFromIndex: Integer;
    procedure RefreshRules;
    procedure Preview;
  public
    { Public-Deklarationen }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses
  uFormRuleNCD;

procedure TfrmMain.btSaveClick(Sender: TObject);
begin
  if sdFile.Execute then
  begin
    meOutput.Lines.SaveToFile(sdFile.FileName);
  end;
end;

procedure TfrmMain.btStartClick(Sender: TObject);
begin
  Preview;
end;

procedure TfrmMain.btOpenClick(Sender: TObject);
begin
  if odFile.Execute then
  begin
    meInput.Lines.LoadFromFile(odFile.FileName);
    Preview;
  end;
end;

procedure TfrmMain.btAddRuleClick(Sender: TObject);
var
  tp: ITextProcessor;
begin
  Application.CreateForm(TfrmRuleNCD, frmRuleNCD);
  if frmRuleNCD.ShowModal = mrOK then
  begin
    tp := frmRuleNCD.TextProcessor;
    tp.Active := true;
    fRules.Add(tp);
    RefreshRules;
    Preview;
  end;
  frmRuleNCD.Free;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  fRules := TList<ITextProcessor>.Create;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  fRules.Free;
end;

procedure TfrmMain.lbRulesClick(Sender: TObject);
begin
  Preview;
end;

procedure TfrmMain.lbRulesClickCheck(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to lbRules.Count - 1 do
  begin
    fRules[i].Active := lbRules.Checked[i];
  end;
  Preview;
end;

procedure TfrmMain.lbRulesDblClick(Sender: TObject);
begin
  if lbRules.ItemIndex >= 0 then
  begin
    Application.CreateForm(TfrmRuleNCD, frmRuleNCD);
    frmRuleNCD.SetTextProcessor(fRules[lbRules.ItemIndex]);
    if frmRuleNCD.ShowModal = mrOK then
    begin
      fRules[lbRules.ItemIndex] := frmRuleNCD.TextProcessor;
      fRules[lbRules.ItemIndex].Active := true;
      RefreshRules;
      Preview;
    end;
    frmRuleNCD.Free;
  end;
end;

procedure TfrmMain.lbRulesDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  pt: TPoint;
  tp: ITextProcessor;
  targetIndex: Integer;
  i: Integer;
begin
  if fFromIndex >= 0 then
  begin
    pt.X := X;
    pt.Y := Y;
    targetIndex := lbRules.ItemAtPos(pt, false);
    if (targetIndex >= 0) and (targetIndex < lbRules.Count) then
    begin
      if fFromIndex < targetIndex then
      begin
        for i := fFromIndex to targetIndex - 1 do
        begin
          tp := fRules[i];
          fRules[i] := fRules[i + 1];
          fRules[i + 1] := tp;
        end;
      end
      else if targetIndex < fFromIndex then
      begin
        for i := fFromIndex -1 downto targetIndex do
        begin
          tp := fRules[i];
          fRules[i] := fRules[i + 1];
          fRules[i + 1] := tp;
        end;
      end;
      RefreshRules;
      Preview;
    end;
  end;
end;

procedure TfrmMain.lbRulesDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept := Source = lbRules;
end;

procedure TfrmMain.lbRulesKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DELETE then
  begin
    if lbRules.ItemIndex >= 0 then
    begin
      fRules.Delete(lbRules.ItemIndex);
      RefreshRules;
      Preview;
    end;
  end;
end;

procedure TfrmMain.lbRulesStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  fFromIndex := lbRules.ItemIndex;
end;

procedure TfrmMain.meInputChange(Sender: TObject);
begin
  Preview;
end;

procedure TfrmMain.meInputKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = Ord('A')) and (ssCtrl in Shift) then
  begin
    TMemo(Sender).SelectAll;
    Key := 0;
  end;
end;

procedure TfrmMain.Preview;
var
  tp: ITextProcessor;
  i: Integer;
  l: string;
begin
  for tp in fRules do
  begin
    tp.Start;
  end;
  meOutput.Lines.BeginUpdate;
  meOutput.Lines.Clear;
  for i := 0 to meInput.Lines.Count - 1 do
  begin
    l := meInput.Lines[i];
    for tp in fRules do
    begin
      if tp.Active then
        l := tp.Process(l);
    end;
    meOutput.Lines.Add(l);
  end;
  meOutput.Lines.EndUpdate;
end;

procedure TfrmMain.RefreshRules;
var
  i: Integer;
begin
  lbRules.Items.BeginUpdate;
  lbRules.Items.Clear;
  for i := 0 to fRules.Count - 1 do
  begin
    lbRules.Items.Add(fRules[i].ToString);
    lbRules.Checked[i] := fRules[i].Active;
  end;
  lbRules.Items.EndUpdate;
end;

end.
