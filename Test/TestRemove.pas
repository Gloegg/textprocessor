unit TestRemove;
{

  Delphi DUnit-Testfall
  ----------------------
  Diese Unit enth�lt ein Skeleton einer Testfallklasse, das vom Experten f�r Testf�lle erzeugt wurde.
  �ndern Sie den erzeugten Code so, dass er die Methoden korrekt einrichtet und aus der 
  getesteten Unit aufruft.

}

interface

uses
  TestFramework, uTextProcessor;

type
  // Testmethoden f�r Klasse TRemove

  TestTRemove = class(TTestCase)
  strict private
    FRemove: TRemove;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestSimpleAll;
    procedure TestSimpleFirst;
    procedure TestSimpleLast;
    procedure TestCaseSensitiveAll;
    procedure TestCaseSensitiveFirst;
    procedure TestCaseSensitiveLast;
    procedure TestSimpleWildcards;
  end;

implementation

procedure TestTRemove.SetUp;
begin
  FRemove := TRemove.Create;
end;

procedure TestTRemove.TearDown;
begin
  FRemove.Free;
  FRemove := nil;
end;

procedure TestTRemove.TestCaseSensitiveAll;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'aaBBaabbccbbddBb';
  FRemove.Text := 'bb';
  FRemove.CaseSensitive := true;
  FRemove.UseWildcards := false;
  FRemove.Occurrences := occAll;
  ReturnValue := FRemove.Process(aInput);
  CheckEqualsString('aaBBaaccddBb', ReturnValue);
end;

procedure TestTRemove.TestCaseSensitiveFirst;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'aaBBaabbccbbddBb';
  FRemove.Text := 'bb';
  FRemove.CaseSensitive := true;
  FRemove.UseWildcards := false;
  FRemove.Occurrences := occFirst;
  ReturnValue := FRemove.Process(aInput);
  CheckEqualsString('aaBBaaccbbddBb', ReturnValue);
end;

procedure TestTRemove.TestCaseSensitiveLast;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'aaBBaabbccbbddBb';
  FRemove.Text := 'bb';
  FRemove.CaseSensitive := true;
  FRemove.UseWildcards := false;
  FRemove.Occurrences := occLast;
  ReturnValue := FRemove.Process(aInput);
  CheckEqualsString('aaBBaabbccddBb', ReturnValue);
end;

procedure TestTRemove.TestSimpleAll;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'aabbaabbccdd';
  FRemove.Text := 'bb';
  FRemove.CaseSensitive := false;
  FRemove.UseWildcards := false;
  FRemove.Occurrences := occAll;
  ReturnValue := FRemove.Process(aInput);
  CheckEqualsString('aaaaccdd', ReturnValue);
end;

procedure TestTRemove.TestSimpleFirst;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'aabbaabbccdd';
  FRemove.Text := 'bb';
  FRemove.CaseSensitive := false;
  FRemove.UseWildcards := false;
  FRemove.Occurrences := occFirst;
  ReturnValue := FRemove.Process(aInput);
  CheckEqualsString('aaaabbccdd', ReturnValue);
end;

procedure TestTRemove.TestSimpleLast;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'aabbaabbccdd';
  FRemove.Text := 'bb';
  FRemove.CaseSensitive := false;
  FRemove.UseWildcards := false;
  FRemove.Occurrences := occLast;
  ReturnValue := FRemove.Process(aInput);
  CheckEqualsString('aabbaaccdd', ReturnValue);
end;

procedure TestTRemove.TestSimpleWildcards;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo bar foO fob baz';
  FRemove.Text := 'fo?';
  FRemove.UseWildcards := true;
  ReturnValue := FRemove.Process(aInput);
  CheckEqualsString(' bar   baz', ReturnValue);
end;

initialization
  // Alle Testf�lle beim Testprogramm registrieren
  RegisterTest(TestTRemove.Suite);
end.

