unit TestSerialize;
{

  Delphi DUnit-Testfall
  ----------------------
  Diese Unit enth�lt ein Skeleton einer Testfallklasse, das vom Experten f�r Testf�lle erzeugt wurde.
  �ndern Sie den erzeugten Code so, dass er die Methoden korrekt einrichtet und aus der 
  getesteten Unit aufruft.

}

interface

uses
  TestFramework, uTextProcessor;

type
  // Testmethoden f�r Klasse TSerialize

  TestTSerialize = class(TTestCase)
  strict private
    FSerialize: TSerialize;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestPrefix;
    procedure TestBiggerStep;
    procedure TestPadded;
    procedure TestPaddedBigStep;
    procedure TestSuffix;
    procedure TestPosition;
  end;

implementation

procedure TestTSerialize.SetUp;
begin
  FSerialize := TSerialize.Create;
end;

procedure TestTSerialize.TearDown;
begin
  FSerialize.Free;
  FSerialize := nil;
end;

procedure TestTSerialize.TestBiggerStep;
var
  ReturnValue: string;
  aInput: string;
  i: Integer;
begin
  aInput := 'xxx';
  FSerialize.IndexStart := 1;
  FSerialize.IndexStep := 3;
  FSerialize.Position := spPrefix;
  FSerialize.Start;
  for i := 1 to 5 do
    ReturnValue := FSerialize.Process(aInput);
  CheckEqualsString('13xxx', ReturnValue);
end;

procedure TestTSerialize.TestPadded;
var
  ReturnValue: string;
  aInput: string;
  i: Integer;
begin
  aInput := 'xxx';
  FSerialize.IndexStart := 1;
  FSerialize.IndexStep := 1;
  FSerialize.Position := spPrefix;
  FSerialize.Pad := true;
  FSerialize.PadLength := 4;
  FSerialize.Start;
  for i := 1 to 5 do
    ReturnValue := FSerialize.Process(aInput);
  CheckEqualsString('0005xxx', ReturnValue);
end;

procedure TestTSerialize.TestPaddedBigStep;
var
  ReturnValue: string;
  aInput: string;
  i: Integer;
begin
  aInput := 'xxx';
  FSerialize.IndexStart := 0;
  FSerialize.IndexStep := 100;
  FSerialize.Position := spPrefix;
  FSerialize.Pad := true;
  FSerialize.PadLength := 3;
  FSerialize.Start;
  for i := 1 to 5 do
    ReturnValue := FSerialize.Process(aInput);
  CheckEqualsString('400xxx', ReturnValue);
end;

procedure TestTSerialize.TestPosition;
var
  ReturnValue: string;
  aInput: string;
  i: Integer;
begin
  aInput := 'xxxx';
  FSerialize.IndexStart := 1;
  FSerialize.IndexStep := 1;
  FSerialize.Position := spExact;
  FSerialize.PositionIndex := 2;
  FSerialize.Start;
  for i := 1 to 5 do
    ReturnValue := FSerialize.Process(aInput);
  CheckEqualsString('xx5xx', ReturnValue);
end;

procedure TestTSerialize.TestPrefix;
var
  ReturnValue: string;
  aInput: string;
  i: Integer;
begin
  aInput := 'xxx';
  FSerialize.IndexStart := 1;
  FSerialize.IndexStep := 1;
  FSerialize.Position := spPrefix;
  FSerialize.Start;
  for i := 1 to 5 do
    ReturnValue := FSerialize.Process(aInput);
  CheckEqualsString('5xxx', ReturnValue);
end;

procedure TestTSerialize.TestSuffix;
var
  ReturnValue: string;
  aInput: string;
  i: Integer;
begin
  aInput := 'xxx';
  FSerialize.IndexStart := 1;
  FSerialize.IndexStep := 1;
  FSerialize.Position := spSuffix;
  FSerialize.Start;
  for i := 1 to 5 do
    ReturnValue := FSerialize.Process(aInput);
  CheckEqualsString('xxx5', ReturnValue);
end;

initialization
  // Alle Testf�lle beim Testprogramm registrieren
  RegisterTest(TestTSerialize.Suite);
end.

