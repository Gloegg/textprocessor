unit TestCleanUp;
{

  Delphi DUnit-Testfall
  ----------------------
  Diese Unit enth�lt ein Skeleton einer Testfallklasse, das vom Experten f�r Testf�lle erzeugt wurde.
  �ndern Sie den erzeugten Code so, dass er die Methoden korrekt einrichtet und aus der 
  getesteten Unit aufruft.

}

interface

uses
  TestFramework, uTextProcessor;

type
  // Testmethoden f�r Klasse TCleanUp

  TestTCleanUp = class(TTestCase)
  strict private
    FCleanUp: TCleanUp;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestFixSpaces;
    procedure TestReplaceDot;
    procedure TestReplaceComma;
    procedure TestReplaceMinus;
    procedure TestReplacePlus;
    procedure TestReplaceUnderscore;
    procedure TestStripBraces;
    procedure TestStripBrackets;
    procedure TestStripCurlyBraces;
  end;

implementation

procedure TestTCleanUp.SetUp;
begin
  FCleanUp := TCleanUp.Create;
end;

procedure TestTCleanUp.TearDown;
begin
  FCleanUp.Free;
  FCleanUp := nil;
end;

procedure TestTCleanUp.TestFixSpaces;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := '  aaa   bbb ccc def ';
  FCleanUp.FixSpaces := true;
  ReturnValue := FCleanUp.Process(aInput);
  CheckEqualsString('aaa bbb ccc def',ReturnValue);
end;

procedure TestTCleanUp.TestReplaceComma;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'a.b,c-d+e_f';
  FCleanUp.ReplaceComma := true;
  ReturnValue := FCleanUp.Process(aInput);
  CheckEqualsString('a.b c-d+e_f',ReturnValue);
end;

procedure TestTCleanUp.TestReplaceDot;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'a.b,c-d+e_f';
  FCleanUp.ReplaceDots := true;
  ReturnValue := FCleanUp.Process(aInput);
  CheckEqualsString('a b,c-d+e_f',ReturnValue);
end;

procedure TestTCleanUp.TestReplaceMinus;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'a.b,c-d+e_f';
  FCleanUp.ReplaceMinus := true;
  ReturnValue := FCleanUp.Process(aInput);
  CheckEqualsString('a.b,c d+e_f',ReturnValue);
end;

procedure TestTCleanUp.TestReplacePlus;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'a.b,c-d+e_f';
  FCleanUp.ReplacePlus := true;
  ReturnValue := FCleanUp.Process(aInput);
  CheckEqualsString('a.b,c-d e_f',ReturnValue);
end;

procedure TestTCleanUp.TestReplaceUnderscore;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'a.b,c-d+e_f';
  FCleanUp.ReplaceUnderscore := true;
  ReturnValue := FCleanUp.Process(aInput);
  CheckEqualsString('a.b,c-d+e f',ReturnValue);
end;

procedure TestTCleanUp.TestStripBraces;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo (bar) baz (foo)';
  FCleanUp.StripBraces := true;
  ReturnValue := FCleanUp.Process(aInput);
  CheckEqualsString('foo  baz ',ReturnValue);
end;

procedure TestTCleanUp.TestStripBrackets;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo [bar] baz [foo]';
  FCleanUp.StripSquareBrackets := true;
  ReturnValue := FCleanUp.Process(aInput);
  CheckEqualsString('foo  baz ',ReturnValue);
end;

procedure TestTCleanUp.TestStripCurlyBraces;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo {bar} baz {foo}';
  FCleanUp.StripCurlyBraces := true;
  ReturnValue := FCleanUp.Process(aInput);
  CheckEqualsString('foo  baz ',ReturnValue);
end;

initialization
  // Alle Testf�lle beim Testprogramm registrieren
  RegisterTest(TestTCleanUp.Suite);
end.

