unit TestRearrange;
{

  Delphi DUnit-Testfall
  ----------------------
  Diese Unit enth�lt ein Skeleton einer Testfallklasse, das vom Experten f�r Testf�lle erzeugt wurde.
  �ndern Sie den erzeugten Code so, dass er die Methoden korrekt einrichtet und aus der 
  getesteten Unit aufruft.

}

interface

uses
  TestFramework, uTextProcessor;

type
  // Testmethoden f�r Klasse TRearrange

  TestTRearrange = class(TTestCase)
  strict private
    FRearrange: TRearrange;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestSplittersSimple;
    procedure TestPositionsSimple;
  end;

implementation

procedure TestTRearrange.SetUp;
begin
  FRearrange := TRearrange.Create;
end;

procedure TestTRearrange.TearDown;
begin
  FRearrange.Free;
  FRearrange := nil;
end;

procedure TestTRearrange.TestPositionsSimple;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'aaabbb';
  FRearrange.Option := raoPositions;
  FRearrange.Splitters := '3';
  FRearrange.Pattern := '$1 - $0';
  ReturnValue := FRearrange.Process(aInput);
  CheckEqualsString('bbb - aaa',ReturnValue);
end;

procedure TestTRearrange.TestSplittersSimple;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'aaa - bbb';
  FRearrange.Option := raoDelimiters;
  FRearrange.Splitters := ' - ';
  FRearrange.Pattern := '$1 - $0';
  ReturnValue := FRearrange.Process(aInput);
  CheckEqualsString('bbb - aaa',ReturnValue);
end;

initialization
  // Alle Testf�lle beim Testprogramm registrieren
  RegisterTest(TestTRearrange.Suite);
end.

