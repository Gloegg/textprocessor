unit TestReplace;
{

  Delphi DUnit-Testfall
  ----------------------
  Diese Unit enth�lt ein Skeleton einer Testfallklasse, das vom Experten f�r Testf�lle erzeugt wurde.
  �ndern Sie den erzeugten Code so, dass er die Methoden korrekt einrichtet und aus der 
  getesteten Unit aufruft.

}

interface

uses
  TestFramework, uTextProcessor, Generics.Collections;

type
  // Testmethoden f�r Klasse TReplace

  TestTReplace = class(TTestCase)
  strict private
    FReplace: TReplace;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestReplaceAll;
    procedure TestReplaceFirst;
    procedure TestReplaceLast;
    procedure TestCaseSensitive;
  end;

implementation

procedure TestTReplace.SetUp;
begin
  FReplace := TReplace.Create;
end;

procedure TestTReplace.TearDown;
begin
  FReplace.Free;
  FReplace := nil;
end;

procedure TestTReplace.TestCaseSensitive;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo bar baz bar BAR';
  FReplace.SearchText := 'bar';
  FReplace.ReplaceText := 'xxx';
  FReplace.Occurrences := occAll;
  FReplace.CaseSensitive := true;
  ReturnValue := FReplace.Process(aInput);
  CheckEqualsString('foo xxx baz xxx BAR',ReturnValue);
end;

procedure TestTReplace.TestReplaceAll;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo bar baz bar BAR';
  FReplace.SearchText := 'bar';
  FReplace.ReplaceText := 'xxx';
  FReplace.Occurrences := occAll;
  ReturnValue := FReplace.Process(aInput);
  CheckEqualsString('foo xxx baz xxx xxx',ReturnValue);
end;

procedure TestTReplace.TestReplaceFirst;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo bar baz bar BAR';
  FReplace.SearchText := 'bar';
  FReplace.ReplaceText := 'xxx';
  FReplace.Occurrences := occFirst;
  ReturnValue := FReplace.Process(aInput);
  CheckEqualsString('foo xxx baz bar BAR',ReturnValue);
end;

procedure TestTReplace.TestReplaceLast;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo bar baz bar BAR';
  FReplace.SearchText := 'bar';
  FReplace.ReplaceText := 'xxx';
  FReplace.Occurrences := occLast;
  ReturnValue := FReplace.Process(aInput);
  CheckEqualsString('foo bar baz bar xxx',ReturnValue);
end;

initialization
  // Alle Testf�lle beim Testprogramm registrieren
  RegisterTest(TestTReplace.Suite);
end.

