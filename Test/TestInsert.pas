unit TestInsert;
{

  Delphi DUnit-Testfall
  ----------------------
  Diese Unit enth�lt ein Skeleton einer Testfallklasse, das vom Experten f�r Testf�lle erzeugt wurde.
  �ndern Sie den erzeugten Code so, dass er die Methoden korrekt einrichtet und aus der 
  getesteten Unit aufruft.

}

interface

uses
  TestFramework, uTextProcessor;

type
  // Testmethoden f�r Klasse TInsert

  TestTInsert = class(TTestCase)
  strict private
    FInsert: TInsert;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestPrefix;
    procedure TestSuffix;
    procedure TestExact;
    procedure TestAfter;
    procedure TestBefore;
    procedure TestAfterMissing;
    procedure TestBeforeMissing;
  end;

implementation

procedure TestTInsert.SetUp;
begin
  FInsert := TInsert.Create;
end;

procedure TestTInsert.TearDown;
begin
  FInsert.Free;
  FInsert := nil;
end;

procedure TestTInsert.TestAfter;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo';
  FInsert.Text := 'bar';
  FInsert.Position := posAfter;
  FInsert.PositionText := 'fo';
  ReturnValue := FInsert.Process(aInput);
  Check(ReturnValue = 'fobaro');
end;

procedure TestTInsert.TestAfterMissing;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo';
  FInsert.Text := 'bar';
  FInsert.Position := posAfter;
  FInsert.PositionText := 'xx';
  ReturnValue := FInsert.Process(aInput);
  Check(ReturnValue = 'foo');
end;

procedure TestTInsert.TestBefore;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo';
  FInsert.Text := 'bar';
  FInsert.Position := posBefore;
  FInsert.PositionText := 'xx';
  ReturnValue := FInsert.Process(aInput);
  Check(ReturnValue = 'foo');
end;

procedure TestTInsert.TestBeforeMissing;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo';
  FInsert.Text := 'bar';
  FInsert.Position := posBefore;
  FInsert.PositionText := 'xx';
  ReturnValue := FInsert.Process(aInput);
  Check(ReturnValue = 'foo');
end;

procedure TestTInsert.TestExact;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo';
  FInsert.Text := 'bar';
  FInsert.Position := posExact;
  FInsert.PositionIndex := 2;
  ReturnValue := FInsert.Process(aInput);
  Check(ReturnValue = 'fobaro');
end;

procedure TestTInsert.TestPrefix;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'bbb';
  FInsert.Text := 'aaa';
  FInsert.Position := posPrefix;
  ReturnValue := FInsert.Process(aInput);
  Check(ReturnValue = 'aaabbb');
end;

procedure TestTInsert.TestSuffix;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'bbb';
  FInsert.Text := 'aaa';
  FInsert.Position := posSuffix;
  ReturnValue := FInsert.Process(aInput);
  Check(ReturnValue = 'bbbaaa');
end;

initialization
  // Alle Testf�lle beim Testprogramm registrieren
  RegisterTest(TestTInsert.Suite);
end.

