unit TestDelete;
{

  Delphi DUnit-Testfall
  ----------------------
  Diese Unit enth�lt ein Skeleton einer Testfallklasse, das vom Experten f�r Testf�lle erzeugt wurde.
  �ndern Sie den erzeugten Code so, dass er die Methoden korrekt einrichtet und aus der 
  getesteten Unit aufruft.

}

interface

uses
  TestFramework, uTextProcessor;

type
  // Testmethoden f�r Klasse TDelete

  TestTDelete = class(TTestCase)
  strict private
    FDelete: TDelete;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestFromPosCount;
    procedure TestFromDelimCountKeep;
    procedure TestFromPosToDelimKeep;
    procedure TestFromDelimToDelimKeep;
    procedure TestFromPosToEnd;
    procedure TestFromDelimToEndKeep;
    procedure TestFromDelimCount;
    procedure TestFromPosToDelim;
    procedure TestFromDelimToDelim;
    procedure TestFromDelimToEnd;
  end;

implementation

procedure TestTDelete.SetUp;
begin
  FDelete := TDelete.Create;
end;

procedure TestTDelete.TearDown;
begin
  FDelete.Free;
  FDelete := nil;
end;

procedure TestTDelete.TestFromDelimCount;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'abcdefgh';
  FDelete.From.Option := dfoDelimiter;
  FDelete.From.Delimiter := 'cd';
  FDelete.Till.Option := duoCount;
  FDelete.Till.Count := 3;
  FDelete.DeleteDelimiters := true;
  ReturnValue := FDelete.Process(aInput);
  CheckEqualsString('abh', ReturnValue);
end;

procedure TestTDelete.TestFromDelimCountKeep;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'abcdefgh';
  FDelete.From.Option := dfoDelimiter;
  FDelete.From.Delimiter := 'cd';
  FDelete.Till.Option := duoCount;
  FDelete.Till.Count := 3;
  ReturnValue := FDelete.Process(aInput);
  CheckEqualsString('abcdh', ReturnValue);
end;

procedure TestTDelete.TestFromDelimToDelim;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'abcdefgh';
  FDelete.From.Option := dfoDelimiter;
  FDelete.From.Delimiter := 'cd';
  FDelete.Till.Option := duoDelimiter;
  FDelete.Till.Delimiter := 'fg';
  FDelete.DeleteDelimiters := true;
  ReturnValue := FDelete.Process(aInput);
  CheckEqualsString('abh', ReturnValue);
end;

procedure TestTDelete.TestFromDelimToDelimKeep;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'abcdefgh';
  FDelete.From.Option := dfoDelimiter;
  FDelete.From.Delimiter := 'cd';
  FDelete.Till.Option := duoDelimiter;
  FDelete.Till.Delimiter := 'fg';
  ReturnValue := FDelete.Process(aInput);
  CheckEqualsString('abcdfgh', ReturnValue);
end;

procedure TestTDelete.TestFromDelimToEnd;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'abcdefgh';
  FDelete.From.Option := dfoDelimiter;
  FDelete.From.Delimiter := 'cd';
  FDelete.Till.Option := duoTillTheEnd;
  FDelete.DeleteDelimiters := True;
  ReturnValue := FDelete.Process(aInput);
  CheckEqualsString('ab', ReturnValue);
end;

procedure TestTDelete.TestFromDelimToEndKeep;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'abcdefgh';
  FDelete.From.Option := dfoDelimiter;
  FDelete.From.Delimiter := 'cd';
  FDelete.Till.Option := duoTillTheEnd;
  ReturnValue := FDelete.Process(aInput);
  CheckEqualsString('abcd', ReturnValue);
end;

procedure TestTDelete.TestFromPosCount;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'abcdefgh';
  FDelete.From.Option := dfoPosition;
  FDelete.From.Position := 3;
  FDelete.Till.Option := duoCount;
  FDelete.Till.Count := 4;
  ReturnValue := FDelete.Process(aInput);
  CheckEqualsString('abgh', ReturnValue);
end;

procedure TestTDelete.TestFromPosToDelim;
begin

end;

procedure TestTDelete.TestFromPosToDelimKeep;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'abcdefgh';
  FDelete.From.Option := dfoPosition;
  FDelete.From.Position := 3;
  FDelete.Till.Option := duoDelimiter;
  FDelete.Till.Delimiter := 'fg';
  ReturnValue := FDelete.Process(aInput);
  CheckEqualsString('abfgh', ReturnValue);
end;

procedure TestTDelete.TestFromPosToEnd;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'abcdefgh';
  FDelete.From.Option := dfoPosition;
  FDelete.From.Position := 3;
  FDelete.Till.Option := duoTillTheEnd;
  ReturnValue := FDelete.Process(aInput);
  CheckEqualsString('ab', ReturnValue);
end;

initialization
  // Alle Testf�lle beim Testprogramm registrieren
  RegisterTest(TestTDelete.Suite);
end.

