unit TestStrip;
{

  Delphi DUnit-Testfall
  ----------------------
  Diese Unit enth�lt ein Skeleton einer Testfallklasse, das vom Experten f�r Testf�lle erzeugt wurde.
  �ndern Sie den erzeugten Code so, dass er die Methoden korrekt einrichtet und aus der 
  getesteten Unit aufruft.

}

interface

uses
  TestFramework, uTextProcessor;

type
  // Testmethoden f�r Klasse TStrip

  TestTStrip = class(TTestCase)
  strict private
    FStrip: TStrip;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestStrip;
    procedure TestStripInvert;
    procedure TestSymbols;
  end;

implementation

procedure TestTStrip.SetUp;
begin
  FStrip := TStrip.Create;
end;

procedure TestTStrip.TearDown;
begin
  FStrip.Free;
  FStrip := nil;
end;

procedure TestTStrip.TestStrip;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'a%b&c$d';
  FStrip.Chars := '$%&';
  FStrip.StripUser := true;
  ReturnValue := FStrip.Process(aInput);
  CheckEqualsString('abcd',ReturnValue);
end;

procedure TestTStrip.TestStripInvert;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'a%b&c$d';
  FStrip.Chars := '$%&';
  FStrip.Invert := true;
  FStrip.StripUser := true;
  ReturnValue := FStrip.Process(aInput);
  CheckEqualsString('%&$',ReturnValue);
end;

procedure TestTStrip.TestSymbols;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'abc!"�''$%&/=def+-_.,;:#''*~ghi';
  FStrip.StripSymbols := true;
  ReturnValue := FStrip.Process(aInput);
  CheckEqualsString('abcdefghi',ReturnValue);
end;

initialization
  // Alle Testf�lle beim Testprogramm registrieren
  RegisterTest(TestTStrip.Suite);
end.

