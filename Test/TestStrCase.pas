unit TestStrCase;
{

  Delphi DUnit-Testfall
  ----------------------
  Diese Unit enth�lt ein Skeleton einer Testfallklasse, das vom Experten f�r Testf�lle erzeugt wurde.
  �ndern Sie den erzeugten Code so, dass er die Methoden korrekt einrichtet und aus der 
  getesteten Unit aufruft.

}

interface

uses
  TestFramework, uTextProcessor, Generics.Collections;

type
  // Testmethoden f�r Klasse TCase

  TestTCase = class(TTestCase)
  strict private
    FCase: TCase;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestToUpper;
    procedure TestToLower;
    procedure TestEveryWord;
    procedure TestInvert;
    procedure TestFirstLetter;
    procedure TestPreserve;
    procedure TestToUpperIgnore;
    procedure TestToLowerIgnore;
    procedure TestEveryWordIgnore;
    procedure TestInvertIgnore;
    procedure TestFirstLetterIgnore;
    procedure TestPreserveIgnore;
  end;

implementation

procedure TestTCase.SetUp;
begin
  FCase := TCase.Create;
end;

procedure TestTCase.TearDown;
begin
  FCase.Free;
  FCase := nil;
end;

procedure TestTCase.TestEveryWord;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo BAR Blub 123';
  FCase.Mode := cmEveryWord;
  ReturnValue := FCase.Process(aInput);
  CheckEqualsString('Foo Bar Blub 123',ReturnValue);
end;

procedure TestTCase.TestEveryWordIgnore;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo BAR Blub 123';
  FCase.Mode := cmEveryWord;
  FCase.IgnoreText := 'FoO,bAr';
  ReturnValue := FCase.Process(aInput);
  CheckEqualsString('FoO bAr Blub 123',ReturnValue);
end;

procedure TestTCase.TestFirstLetter;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'abcDEFghi123,.-';
  FCase.Mode := cmFirstLetter;
  ReturnValue := FCase.Process(aInput);
  CheckEqualsString('Abcdefghi123,.-',ReturnValue);
end;

procedure TestTCase.TestFirstLetterIgnore;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo bar BAZ';
  FCase.Mode := cmFirstLetter;
  FCase.IgnoreText := 'BAZ';
  ReturnValue := FCase.Process(aInput);
  CheckEqualsString('Foo bar BAZ',ReturnValue);
end;

procedure TestTCase.TestInvert;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'abcDEFghi123,.-';
  FCase.Mode := cmInvert;
  ReturnValue := FCase.Process(aInput);
  CheckEqualsString('ABCdefGHI123,.-',ReturnValue);
end;

procedure TestTCase.TestInvertIgnore;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'FoO bAr baz aCMe';
  FCase.Mode := cmInvert;
  FCase.IgnoreText := 'foo,ACME';
  ReturnValue := FCase.Process(aInput);
  CheckEqualsString('foo BaR BAZ ACME',ReturnValue);
end;

procedure TestTCase.TestPreserve;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo BAR BluB 123';
  FCase.Mode := cmPreserve;
  ReturnValue := FCase.Process(aInput);
  CheckEqualsString('Foo BAR BluB 123',ReturnValue);
end;

procedure TestTCase.TestPreserveIgnore;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'foo BAR bluB baz';
  FCase.Mode := cmPreserve;
  FCase.IgnoreText := 'bar,bAZ';
  ReturnValue := FCase.Process(aInput);
  CheckEqualsString('Foo bar BluB bAZ',ReturnValue);
end;

procedure TestTCase.TestToLower;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'abcDEFghi123,.-';
  FCase.Mode := cmLower;
  ReturnValue := FCase.Process(aInput);
  CheckEqualsString('abcdefghi123,.-',ReturnValue);
end;

procedure TestTCase.TestToLowerIgnore;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'FOO Bar baz bLuB';
  FCase.Mode := cmLower;
  FCase.IgnoreText := 'bAR,baZ,nope';
  ReturnValue := FCase.Process(aInput);
  CheckEqualsString('foo bAR baZ blub',ReturnValue);
end;

procedure TestTCase.TestToUpper;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'abcDEFghi123,.-';
  FCase.Mode := cmUpper;
  ReturnValue := FCase.Process(aInput);
  CheckEqualsString('ABCDEFGHI123,.-',ReturnValue);
end;

procedure TestTCase.TestToUpperIgnore;
var
  ReturnValue: string;
  aInput: string;
begin
  aInput := 'FoO bar bAZ bLuBbLuB';
  FCase.Mode := cmUpper;
  FCase.IgnoreText := 'fOo,bLuB';
  ReturnValue := FCase.Process(aInput);
  CheckEqualsString('fOo BAR BAZ BLUBBLUB',ReturnValue);
end;

initialization
  // Alle Testf�lle beim Testprogramm registrieren
  RegisterTest(TestTCase.Suite);
end.

