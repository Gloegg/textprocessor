program TextProcessorTests;
{

  Delphi DUnit-Testprojekt
  -------------------------
  Dieses Projekt enth�lt das DUnit-Test-Framework und die GUI/Konsolen-Test-Runner.
  F�gen Sie den Bedingungen in den Projektoptionen "CONSOLE_TESTRUNNER" hinzu,
  um den Konsolen-Test-Runner zu verwenden.  Ansonsten wird standardm��ig der
  GUI-Test-Runner verwendet.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  DUnitTestRunner,
  TestInsert in 'TestInsert.pas',
  uTextProcessor in '..\uTextProcessor.pas',
  TestDelete in 'TestDelete.pas',
  TestRemove in 'TestRemove.pas',
  TestRearrange in 'TestRearrange.pas',
  TestStrip in 'TestStrip.pas',
  TestCleanUp in 'TestCleanUp.pas',
  TestSerialize in 'TestSerialize.pas',
  TestStrCase in 'TestStrCase.pas',
  TestReplace in 'TestReplace.pas';

{ *.RES}

begin
  DUnitTestRunner.RunRegisteredTests;
end.

