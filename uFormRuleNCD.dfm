object frmRuleNCD: TfrmRuleNCD
  Left = 0
  Top = 0
  Caption = 'frmRuleNCD'
  ClientHeight = 374
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    635
    374)
  PixelsPerInch = 96
  TextHeight = 13
  object lbType: TListBox
    Left = 8
    Top = 8
    Width = 121
    Height = 327
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    Items.Strings = (
      'Insert'
      'Delete'
      'Remove'
      'Replace'
      'Rearrange'
      'Strip'
      'Case'
      'Serialize'
      'Clean Up')
    TabOrder = 0
    OnClick = lbTypeClick
  end
  object pcType: TPageControl
    Left = 135
    Top = 6
    Width = 492
    Height = 331
    ActivePage = pgCleanUp
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    object pgInsert: TTabSheet
      Caption = 'pgInsert'
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label1: TLabel
        Left = 34
        Top = 19
        Width = 22
        Height = 13
        Caption = 'Text'
      end
      object edInsertText: TEdit
        Left = 62
        Top = 16
        Width = 219
        Height = 21
        TabOrder = 0
      end
      object rgInsertPosition: TRadioGroup
        Left = 24
        Top = 43
        Width = 265
        Height = 193
        Caption = 'Where'
        ItemIndex = 0
        Items.Strings = (
          'Prefix'
          'Suffix'
          'Position'
          'After Text'
          'Before Text')
        TabOrder = 1
        OnClick = rgInsertPositionClick
      end
      object edInsertPosition: TSpinEdit
        Left = 160
        Top = 133
        Width = 49
        Height = 22
        Enabled = False
        MaxValue = 0
        MinValue = 0
        TabOrder = 2
        Value = 0
      end
      object edInsertAfter: TEdit
        Left = 160
        Top = 161
        Width = 121
        Height = 21
        Enabled = False
        TabOrder = 3
      end
      object edInsertBefore: TEdit
        Left = 160
        Top = 203
        Width = 121
        Height = 21
        Enabled = False
        TabOrder = 4
      end
    end
    object pgDelete: TTabSheet
      Caption = 'pgDelete'
      ImageIndex = 1
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object rgDeleteFrom: TRadioGroup
        Left = 31
        Top = 16
        Width = 154
        Height = 81
        Caption = 'From'
        ItemIndex = 0
        Items.Strings = (
          'Position'
          'Delimiter')
        TabOrder = 0
        OnClick = rgDeleteFromClick
      end
      object rgDeleteUntil: TRadioGroup
        Left = 191
        Top = 16
        Width = 185
        Height = 105
        Caption = 'Until'
        ItemIndex = 0
        Items.Strings = (
          'Count'
          'Delimiter'
          'Till the end')
        TabOrder = 1
      end
      object cbDeleteDelim: TCheckBox
        Left = 31
        Top = 135
        Width = 97
        Height = 17
        Caption = 'Delete Delimiters'
        TabOrder = 2
      end
      object edDeleteFromPosition: TSpinEdit
        Left = 105
        Top = 35
        Width = 57
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 3
        Value = 0
      end
      object edDeleteUntilCount: TSpinEdit
        Left = 312
        Top = 35
        Width = 57
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 4
        Value = 0
      end
      object edDeleteUntilText: TEdit
        Left = 312
        Top = 63
        Width = 57
        Height = 21
        Enabled = False
        TabOrder = 5
      end
      object edDeleteFromText: TEdit
        Left = 105
        Top = 63
        Width = 57
        Height = 21
        Enabled = False
        TabOrder = 6
      end
    end
    object pgRemove: TTabSheet
      Caption = 'pgRemove'
      ImageIndex = 2
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label10: TLabel
        Left = 24
        Top = 13
        Width = 74
        Height = 13
        Caption = 'Text to remove'
      end
      object edRemoveText: TEdit
        Left = 24
        Top = 32
        Width = 121
        Height = 21
        TabOrder = 0
      end
      object rgRemoveOccurrence: TRadioGroup
        Left = 24
        Top = 59
        Width = 121
        Height = 78
        Caption = 'Occurrences'
        ItemIndex = 0
        Items.Strings = (
          'All'
          'First'
          'Last')
        TabOrder = 1
      end
      object cbRemoveCaseSensitive: TCheckBox
        Left = 24
        Top = 152
        Width = 97
        Height = 17
        Caption = 'Case Sensitive'
        TabOrder = 2
      end
      object cbRemoveWildcards: TCheckBox
        Left = 24
        Top = 175
        Width = 97
        Height = 17
        Caption = 'Use Wildcards'
        Enabled = False
        TabOrder = 3
      end
    end
    object pgReplace: TTabSheet
      Caption = 'pgReplace'
      ImageIndex = 3
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label2: TLabel
        Left = 11
        Top = 30
        Width = 50
        Height = 13
        Caption = 'Search for'
      end
      object Label3: TLabel
        Left = 11
        Top = 57
        Width = 61
        Height = 13
        Caption = 'Replace with'
      end
      object edReplaceSearch: TEdit
        Left = 80
        Top = 27
        Width = 121
        Height = 21
        TabOrder = 0
      end
      object edReplaceText: TEdit
        Left = 80
        Top = 54
        Width = 121
        Height = 21
        TabOrder = 1
      end
      object rgReplaceOccurrences: TRadioGroup
        Left = 80
        Top = 83
        Width = 121
        Height = 78
        Caption = 'Occurrences'
        ItemIndex = 0
        Items.Strings = (
          'All'
          'First'
          'Last')
        TabOrder = 2
      end
      object cbReplaceCaseSensitive: TCheckBox
        Left = 80
        Top = 167
        Width = 97
        Height = 17
        Caption = 'Case Sensitive'
        TabOrder = 3
      end
      object cbReplaceWildcards: TCheckBox
        Left = 80
        Top = 190
        Width = 97
        Height = 17
        Caption = 'Use Wildcards'
        Enabled = False
        TabOrder = 4
      end
    end
    object pgRearrange: TTabSheet
      Caption = 'pgRearrange'
      ImageIndex = 4
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label5: TLabel
        Left = 11
        Top = 112
        Width = 36
        Height = 13
        Caption = 'Pattern'
      end
      object Label7: TLabel
        Left = 239
        Top = 85
        Width = 189
        Height = 13
        Caption = 'Use | to separate delimiters or numbers'
      end
      object Label8: TLabel
        Left = 239
        Top = 112
        Width = 192
        Height = 13
        Caption = 'Use $0, $1, $2 ... $n to reference parts'
      end
      object Label9: TLabel
        Left = 239
        Top = 21
        Width = 181
        Height = 13
        Caption = '(delimiters are not part of the output)'
      end
      object rgRearrangeMode: TRadioGroup
        Left = 48
        Top = 3
        Width = 185
        Height = 73
        Caption = 'Split using'
        ItemIndex = 0
        Items.Strings = (
          'Delimiters'
          'Positions')
        TabOrder = 0
      end
      object edRearrangeSplitter: TEdit
        Left = 48
        Top = 82
        Width = 185
        Height = 21
        TabOrder = 1
      end
      object edRearrangePattern: TEdit
        Left = 48
        Top = 109
        Width = 185
        Height = 21
        TabOrder = 2
      end
    end
    object pgStrip: TTabSheet
      Caption = 'pgStrip'
      ImageIndex = 5
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cbStripInvert: TCheckBox
        Left = 3
        Top = 163
        Width = 150
        Height = 17
        Caption = 'Strip all except selected'
        TabOrder = 0
      end
      object cbStripLetters: TCheckBox
        Left = 3
        Top = 11
        Width = 97
        Height = 17
        Caption = 'Letters'
        TabOrder = 1
      end
      object edStripLetters: TEdit
        Left = 94
        Top = 9
        Width = 171
        Height = 21
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 2
        Text = 'abcdefghijklmnopqrstuvwxyz'
      end
      object cbStripDigits: TCheckBox
        Left = 3
        Top = 38
        Width = 97
        Height = 17
        Caption = 'Digits'
        TabOrder = 3
      end
      object edStripDigits: TEdit
        Left = 94
        Top = 36
        Width = 171
        Height = 21
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 4
        Text = '0123456789'
      end
      object cbStripSymbols: TCheckBox
        Left = 3
        Top = 65
        Width = 97
        Height = 17
        Caption = 'Symbols'
        TabOrder = 5
      end
      object edStripSymbols: TEdit
        Left = 94
        Top = 63
        Width = 171
        Height = 21
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 6
        Text = '!"'#167'$%&/=+-_.,;:#'#39'*~'
      end
      object cbStripBrackets: TCheckBox
        Left = 3
        Top = 92
        Width = 97
        Height = 17
        Caption = 'Brackets'
        TabOrder = 7
      end
      object edStripBrackets: TEdit
        Left = 94
        Top = 90
        Width = 171
        Height = 21
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 8
        Text = '()[]{}'
      end
      object cbStripUser: TCheckBox
        Left = 3
        Top = 119
        Width = 97
        Height = 17
        Caption = 'User Defined'
        TabOrder = 9
      end
      object edStripUser: TEdit
        Left = 94
        Top = 117
        Width = 171
        Height = 21
        TabOrder = 10
      end
    end
    object pgCase: TTabSheet
      Caption = 'pgCase'
      ImageIndex = 6
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label11: TLabel
        Left = 3
        Top = 157
        Width = 263
        Height = 13
        Caption = 'Force Case of the following words (separate by space)'
      end
      object rgCaseMode: TRadioGroup
        Left = 3
        Top = 3
        Width = 263
        Height = 142
        Caption = 'Case'
        ItemIndex = 0
        Items.Strings = (
          'Every Word'
          'Every Word, but preserve existing case'
          'lowercase'
          'UPPERCASE'
          'iNVERT'
          'First letter only')
        TabOrder = 0
      end
      object edCaseIgnore: TEdit
        Left = 3
        Top = 176
        Width = 266
        Height = 21
        TabOrder = 1
      end
    end
    object pgSerialize: TTabSheet
      Caption = 'pgSerialize'
      ImageIndex = 7
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label4: TLabel
        Left = 26
        Top = 27
        Width = 24
        Height = 13
        Caption = 'Start'
      end
      object Label6: TLabel
        Left = 28
        Top = 55
        Width = 22
        Height = 13
        Caption = 'Step'
      end
      object edSerializeStart: TSpinEdit
        Left = 56
        Top = 24
        Width = 65
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 0
        Value = 0
      end
      object edSerializeStep: TSpinEdit
        Left = 56
        Top = 52
        Width = 65
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 1
        Value = 0
      end
      object cbSerializePad: TCheckBox
        Left = 56
        Top = 80
        Width = 137
        Height = 17
        Caption = 'Pad with zeros to length'
        TabOrder = 2
      end
      object edSerializePad: TSpinEdit
        Left = 56
        Top = 103
        Width = 65
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 3
        Value = 0
      end
      object rgSerializeMode: TRadioGroup
        Left = 232
        Top = 24
        Width = 185
        Height = 105
        Caption = 'Insert where'
        ItemIndex = 0
        Items.Strings = (
          'Prefix'
          'Suffix'
          'Position')
        TabOrder = 4
      end
      object edSerializePosition: TSpinEdit
        Left = 328
        Top = 99
        Width = 65
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 5
        Value = 0
      end
    end
    object pgCleanUp: TTabSheet
      Caption = 'pgCleanUp'
      ImageIndex = 8
      TabVisible = False
      object cbCleanUpBraces: TCheckBox
        Left = 32
        Top = 3
        Width = 97
        Height = 17
        Caption = 'Strip (...)'
        TabOrder = 0
      end
      object cbCleanUpSquare: TCheckBox
        Left = 32
        Top = 26
        Width = 105
        Height = 12
        Caption = 'Strip [...]'
        TabOrder = 1
      end
      object cbCleanUpCurly: TCheckBox
        Left = 32
        Top = 44
        Width = 97
        Height = 17
        Caption = 'Strip {...}'
        TabOrder = 2
      end
      object cbCleanUpDots: TCheckBox
        Left = 32
        Top = 83
        Width = 153
        Height = 17
        Caption = 'Replace Dots '#39'.'#39' with '#39' '#39
        TabOrder = 3
      end
      object cbCleanUpFixSpaces: TCheckBox
        Left = 32
        Top = 211
        Width = 97
        Height = 17
        Caption = 'Fix Spaces'
        TabOrder = 4
      end
      object cbCleanUpComma: TCheckBox
        Left = 32
        Top = 106
        Width = 153
        Height = 17
        Caption = 'Replace Comma '#39','#39' with '#39' '#39
        TabOrder = 5
      end
      object cbCleanUpPlus: TCheckBox
        Left = 32
        Top = 129
        Width = 153
        Height = 17
        Caption = 'Replace Plus '#39'+'#39' with '#39' '#39
        TabOrder = 6
      end
      object cbCleanUpMinus: TCheckBox
        Left = 32
        Top = 152
        Width = 153
        Height = 17
        Caption = 'Replace Minus '#39'-'#39' with '#39' '#39
        TabOrder = 7
      end
      object cbCleanUpUnderscore: TCheckBox
        Left = 32
        Top = 175
        Width = 153
        Height = 17
        Caption = 'Replace Underscore '#39'_'#39' with '#39' '#39
        TabOrder = 8
      end
    end
  end
  object btOK: TButton
    Left = 471
    Top = 341
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 2
    OnClick = btOKClick
  end
  object btCancel: TButton
    Left = 552
    Top = 341
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
