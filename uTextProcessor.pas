unit uTextProcessor;

interface

uses
  Generics.Collections;

type
  ITextProcessor = interface
    ['{A8346581-F275-44E7-BE6D-973251C8D6D7}']
    function Process(aInput: string): string;
    function ToString: string;
    function getActive: boolean;
    procedure setActive(const Value: boolean);
    property Active: boolean read getActive write setActive;
    procedure Start;
    function getClassName: String;
  end;

  TTextProcessor = class(TInterfacedObject, ITextProcessor)
  protected
    fActive: boolean;
    function getActive: boolean; virtual;
    procedure setActive(const Value: boolean); virtual;
  public
    function Process(aInput: string): string; virtual;
    function ToString: string; override;
    property Active: boolean read getActive write setActive;
    procedure Start; virtual;
    function getClassName: String; virtual;
  end;

  TPosition = (posPrefix, posSuffix, posExact, posAfter, posBefore);

  TInsert = class(TTextProcessor)
  private
    FText: string;
    FPosition: TPosition;
    FPositionIndex: integer;
    FPositionText: string;
  public
    property Text: string read FText write FText;
    property Position: TPosition read FPosition write FPosition;
    property PositionIndex: integer read FPositionIndex write FPositionIndex;
    property PositionText: string read FPositionText write FPositionText;
    function Process(aInput: string): string; override;
    function ToString: string; override;
  end;

  TDeleteFromOption = (dfoPosition, dfoDelimiter);

  TDeleteFrom = class
  private
    FPosition: integer;
    FDelimiter: string;
    FOption: TDeleteFromOption;
  public
    property Position: integer read FPosition write FPosition;
    property Delimiter: string read FDelimiter write FDelimiter;
    property Option: TDeleteFromOption read FOption write FOption;
  end;

  TDeleteUntilOption = (duoCount, duoDelimiter, duoTillTheEnd);

  TDeleteUntil = class
  private
    FCount: integer;
    FDelimiter: string;
    FOption: TDeleteUntilOption;
  public
    property Count: integer read FCount write FCount;
    property Delimiter: string read FDelimiter write FDelimiter;
    property Option: TDeleteUntilOption read FOption write FOption;
  end;

  TDelete = class(TTextProcessor)
  private
    FFrom: TDeleteFrom;
    FTill: TDeleteUntil;
    FDeleteDelimiters: boolean;
  public
    constructor Create;
    destructor Destroy; override;
    property From: TDeleteFrom read FFrom write FFrom;
    property Till: TDeleteUntil read FTill write FTill;
    property DeleteDelimiters: boolean read FDeleteDelimiters
      write FDeleteDelimiters;
    function Process(aInput: string): string; override;
    function ToString: string; override;
  end;

  TOccurrence = (occAll, occFirst, occLast);

  TRemove = class(TTextProcessor)
  private
    FText: string;
    FCaseSensitive: boolean;
    FUseWildcards: boolean;
    FOccurrences: TOccurrence;
    function CompareStr(const Left, Right: string): boolean;
  public
    property Text: string read FText write FText;
    property CaseSensitive: boolean read FCaseSensitive write FCaseSensitive;
    property UseWildcards: boolean read FUseWildcards write FUseWildcards;
    property Occurrences: TOccurrence read FOccurrences write FOccurrences;
    function Process(aInput: string): string; override;
    function ToString: string; override;
  end;

  TReplace = class(TTextProcessor)
  private
    FSearchText: string;
    FReplaceText: string;
    FOccurrences: TOccurrence;
    FUseWildcards: boolean;
    FCaseSensitive: boolean;
  public
    property SearchText: string read FSearchText write FSearchText;
    property ReplaceText: string read FReplaceText write FReplaceText;
    property CaseSensitive: boolean read FCaseSensitive write FCaseSensitive;
    property UseWildcards: boolean read FUseWildcards write FUseWildcards;
    property Occurrences: TOccurrence read FOccurrences write FOccurrences;
    function Process(aInput: string): string; override;
    function ToString: string; override;
  end;

  TRearrangeOption = (raoDelimiters, raoPositions);

  TRearrange = class(TTextProcessor)
  private
    FSplitters: string;
    FPattern: string;
    FOption: TRearrangeOption;
  public
    property Splitters: string read FSplitters write FSplitters;
    property Pattern: string read FPattern write FPattern;
    property Option: TRearrangeOption read FOption write FOption;
    function Process(aInput: string): string; override;
    function ToString: string; override;
  end;

  TStrip = class(TTextProcessor)
  private
    FChars: string;
    FInvert: boolean;
    FStripLetters: boolean;
    FStripDigits: boolean;
    FStripSymbols: boolean;
    FStripBraces: boolean;
    FStripUser: boolean;
  public
    property Chars: string read FChars write FChars;
    property Invert: boolean read FInvert write FInvert; // Strip all but Chars
    property StripLetters: boolean read FStripLetters write FStripLetters;
    property StripDigits: boolean read FStripDigits write FStripDigits;
    property StripSymbols: boolean read FStripSymbols write FStripSymbols;
    property StripBraces: boolean read FStripBraces write FStripBraces;
    property StripUser: boolean read FStripUser write FStripUser;
    function Process(aInput: string): string; override;
    function ToString: string; override;
  end;

  TCaseMode = (cmEveryWord, cmPreserve, cmLower, cmUpper, cmInvert,
    cmFirstLetter);

  TCase = class(TTextProcessor)
  private
    FMode: TCaseMode;
    FIgnoreText: string;
    function Capitalize(aInput: string; aPreserve: boolean): string;
    function Invert(aInput: Char): Char;
    function CheckIgnore(aInput: string): string;
  public
    property Mode: TCaseMode read FMode write FMode;
    property IgnoreText: string read FIgnoreText write FIgnoreText;
    function Process(aInput: string): string; override;
    function ToString: string; override;
  end;

  TSerializePosition = (spPrefix, spSuffix, spExact);

  TSerialize = class(TTextProcessor)
  private
    FIndexStart: integer;
    FIndexStep: integer;
    FPad: boolean;
    FPadLength: integer;
    FPosition: TSerializePosition;
    FPositionIndex: integer;
    FCurrent: integer;
    procedure Next;
  public
    property IndexStart: integer read FIndexStart write FIndexStart;
    property IndexStep: integer read FIndexStep write FIndexStep;
    property Pad: boolean read FPad write FPad;
    property PadLength: integer read FPadLength write FPadLength;
    property Position: TSerializePosition read FPosition write FPosition;
    property PositionIndex: integer read FPositionIndex write FPositionIndex;
    procedure Start; override;
    function Process(aInput: string): string; override;
    function ToString: string; override;
  end;

  TCleanUp = class(TTextProcessor)
  private
    FStripBraces: boolean;
    FStripSquareBrackets: boolean;
    FStripCurlyBraces: boolean;
    FReplaceDots: boolean;
    FReplaceComma: boolean;
    FReplaceUnderscore: boolean;
    FReplacePlus: boolean;
    FReplaceMinus: boolean;
    FFixSpaces: boolean;
    function DoStripBraces(const aInput: string): string;
    function DoStripSquareBrackets(const aInput: string): string;
    function DoStripCurlyBraces(const aInput: string): string;
    function DoFixSpaces(aInput: string): string;
  public
    property StripBraces: boolean read FStripBraces write FStripBraces;
    property StripSquareBrackets: boolean read FStripSquareBrackets
      write FStripSquareBrackets;
    property StripCurlyBraces: boolean read FStripCurlyBraces
      write FStripCurlyBraces;
    property ReplaceDots: boolean read FReplaceDots write FReplaceDots;
    property ReplaceComma: boolean read FReplaceComma write FReplaceComma;
    property ReplaceUnderscore: boolean read FReplaceUnderscore
      write FReplaceUnderscore;
    property ReplacePlus: boolean read FReplacePlus write FReplacePlus;
    property ReplaceMinus: boolean read FReplaceMinus write FReplaceMinus;
    property FixSpaces: boolean read FFixSpaces write FFixSpaces;
    function Process(aInput: string): string; override;
    function ToString: string; override;
  end;

implementation

uses
  SysUtils, StrUtils, Character, RegularExpressions;

{ TInsert }

function TInsert.Process(aInput: string): string;
var
  p: integer;
begin
  case FPosition of
    posPrefix:
      result := FText + aInput;
    posSuffix:
      result := aInput + FText;
    posExact:
      result := aInput.Insert(FPositionIndex, FText);
    posAfter:
      begin
        p := aInput.IndexOf(FPositionText);
        if p >= 0 then
          result := aInput.Insert(p + FPositionText.Length, FText)
        else
          result := aInput;
      end;
    posBefore:
      begin
        p := aInput.IndexOf(FPositionText);
        if p >= 0 then
          result := aInput.Insert(p, FText)
        else
          result := aInput;
      end;
  end;
end;

function TInsert.ToString: string;
begin
  result := 'Insert ' + FText + ' ';
  case FPosition of
    posPrefix:
      result := result + 'as Prefix';
    posSuffix:
      result := result + 'as Suffix';
    posExact:
      result := result + 'at Position ' + IntToStr(FPositionIndex);
    posAfter:
      result := result + 'after text "' + FPositionText + '"';
    posBefore:
      result := result + 'before text "' + FPositionText + '"';
  end;
end;

{ TDelete }

constructor TDelete.Create;
begin
  FFrom := TDeleteFrom.Create;
  FTill := TDeleteUntil.Create;
end;

destructor TDelete.Destroy;
begin
  FFrom.Free;
  FTill.Free;
  inherited;
end;

function TDelete.Process(aInput: string): string;
var
  pFrom, pTill: integer;
begin
  pFrom := 0;
  case FFrom.Option of
    dfoPosition:
      pFrom := FFrom.Position - 1;
    dfoDelimiter:
      begin
        pFrom := aInput.IndexOf(FFrom.Delimiter);
        if pFrom < 0 then
          Exit(aInput);
        if not FDeleteDelimiters then
          pFrom := pFrom + FFrom.Delimiter.Length;
      end;
  end;
  case FTill.Option of
    duoCount:
      begin
        if FDeleteDelimiters then
          result := aInput.Remove(pFrom, FTill.Count + FFrom.FDelimiter.Length)
        else
          result := aInput.Remove(pFrom, FTill.Count);
      end;
    duoDelimiter:
      begin
        pTill := aInput.IndexOf(FTill.Delimiter);
        if pTill < 0 then
          result := aInput.Remove(pFrom)
        else
        begin
          if FDeleteDelimiters then
            pTill := pTill + FTill.Delimiter.Length;
          result := aInput.Remove(pFrom, pTill - pFrom);
        end;
      end;
    duoTillTheEnd:
      result := aInput.Substring(0, pFrom);
  end;
end;

function TDelete.ToString: string;
begin
  result := 'Delete from ';
  case FFrom.Option of
    dfoPosition:
      result := result + 'position ' + IntToStr(FFrom.Position);
    dfoDelimiter:
      result := result + 'delimiter ' + FFrom.Delimiter;
  end;
  result := result + ' ';
  case FTill.Option of
    duoCount:
      result := result + Format(' %d characters', [FTill.Count]);
    duoDelimiter:
      result := result + ' until delimiter ' + FTill.Delimiter;
    duoTillTheEnd:
      result := result + ' until the end';
  end;
  if FDeleteDelimiters then
    result := result + ' (delete Delimiters)'
  else
    result := result + ' (keep Delimiters)';
end;

{ TRemove }

function TRemove.CompareStr(const Left, Right: string): boolean;
begin

end;

function TRemove.Process(aInput: string): string;
var
  flags: TReplaceFlags;
  p: integer;
  i: integer;
begin
  if not FUseWildcards then
  begin
    if FCaseSensitive then
      flags := []
    else
      flags := [rfIgnoreCase];

    case FOccurrences of
      occAll:
        begin
          flags := flags + [rfReplaceAll];
          result := aInput.Replace(FText, '', flags)
        end;
      occFirst:
        begin
          result := aInput.Replace(FText, '', flags)
        end;
      occLast:
        begin
          p := aInput.LastIndexOf(FText);
          result := aInput.Remove(p, FText.Length);
        end;
    end;
  end
  else
  begin
    for i := 0 to aInput.Length - FText.Length - 1 do
    begin

    end;
  end;
end;

function TRemove.ToString: string;
begin
  result := 'Remove';
  case FOccurrences of
    occAll:
      result := result + ' all ';
    occFirst:
      result := result + ' first ';
    occLast:
      result := result + ' last ';
  end;
  result := result + FText;
  if FCaseSensitive then
    result := result + ' (case sensitive)';
end;

{ TReplace }

function TReplace.Process(aInput: string): string;
var
  flags: TReplaceFlags;
  p: integer;
begin
  if FCaseSensitive then
    flags := []
  else
    flags := [rfIgnoreCase];
  case FOccurrences of
    occAll:
      begin
        flags := flags + [rfReplaceAll];
        result := aInput.Replace(FSearchText, FReplaceText, flags);
      end;
    occFirst:
      begin
        result := aInput.Replace(FSearchText, FReplaceText, flags);
      end;
    occLast:
      begin
        if FCaseSensitive then
          p := aInput.LastIndexOf(FSearchText)
        else
          p := aInput.ToLower.LastIndexOf(FSearchText.ToLower);
        result := aInput.Remove(p, FReplaceText.Length);
        result := result.Insert(p, FReplaceText);
      end;
  end;
end;

function TReplace.ToString: string;
begin
  case FOccurrences of
    occAll:
      result := 'Replace all ' + FSearchText + ' with ' + FReplaceText;
    occFirst:
      result := 'Replace first ' + FSearchText + ' with ' + FReplaceText;
    occLast:
      result := 'Replace last ' + FSearchText + ' with ' + FReplaceText;
  end;
  if FCaseSensitive then
    result := result + ' (case sensitive)';
end;

{ TRearrange }

function TRearrange.Process(aInput: string): string;
var
  Splitters: TArray<string>;
  parts: TList<string>;
  i, p: integer;
  s: string;
begin
  result := FPattern;
  parts := TList<string>.Create;
  case FOption of
    raoDelimiters:
      begin
        Splitters := FSplitters.Split(['|']);
        for s in aInput.Split(Splitters, TStringSplitOptions.None) do
          parts.Add(s);
      end;
    raoPositions:
      begin
        Splitters := FSplitters.Split(['|']);
        for i := 0 to High(Splitters) do
        begin
          p := StrToInt(Splitters[i]);
          parts.Add(aInput.Substring(0, p));
          aInput := aInput.Remove(0, p);
        end;
        if aInput <> '' then
          parts.Add(aInput);
      end;
  end;
  for i := 0 to parts.Count - 1 do
  begin
    result := result.Replace(Format('$%d', [i]), parts[i]);
  end;
  parts.Free;
end;

function TRearrange.ToString: string;
begin
  result := 'Rearrange';
end;

{ TStrip }

function TStrip.Process(aInput: string): string;
const
  letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  digits = '0123456789';
  symbols = '!"�$%&/=+-_.,;:#''*~';
  braces = '()[]{}';
var
  ch: Char;
  Chars: string;
begin
  Chars := '';
  if FStripLetters then
    Chars := Chars + letters;
  if FStripDigits then
    Chars := Chars + digits;
  if FStripSymbols then
    Chars := Chars + symbols;
  if FStripBraces then
    Chars := Chars + braces;
  if FStripUser then
    Chars := Chars + FChars;

  result := '';
  if FInvert then
  begin
    for ch in aInput do
    begin
      if Chars.Contains(ch) then
      begin
        result := result + ch;
      end;
    end;
  end
  else
  begin
    for ch in aInput do
    begin
      if not Chars.Contains(ch) then
      begin
        result := result + ch;
      end;
    end;
  end;
end;

function TStrip.ToString: string;
begin
  if FInvert then
    result := 'Strip ' + FChars + ' from input'
  else
    result := 'Strip all except ' + FChars + ' from input';
end;

{ TCase }

function TCase.Capitalize(aInput: string; aPreserve: boolean): string;
begin
  result := aInput.Substring(0, 1).toUpper;
  if aPreserve then
    result := result + aInput.Substring(1)
  else
    result := result + aInput.Substring(1).ToLower;
end;

function TCase.CheckIgnore(aInput: string): string;
var
  s: string;
begin
  result := aInput;
  for s in FIgnoreText.Split([','], TStringSplitOptions.ExcludeEmpty) do
  begin
    if String.Compare(aInput, s, true) = 0 then
      Exit(s);
  end;
end;

function TCase.Invert(aInput: Char): Char;
begin
  if CharInSet(aInput, ['a' .. 'z']) then
    result := aInput.toUpper // toUpper(aInput)
  else if CharInSet(aInput, ['A' .. 'Z']) then
    result := aInput.ToLower // ToLower(aInput)
  else
    result := aInput;
end;

function TCase.Process(aInput: string): string;
var
  s, tmp: string;
  ch: Char;
begin
  case FMode of
    cmEveryWord:
      begin
        tmp := '';
        for s in aInput.Split([' '], TStringSplitOptions.None) do
        begin
          if tmp <> '' then
            tmp := tmp + ' ';
          tmp := tmp + Capitalize(s, false);
        end;
      end;
    cmPreserve:
      begin
        tmp := '';
        for s in aInput.Split([' '], TStringSplitOptions.None) do
        begin
          if tmp <> '' then
            tmp := tmp + ' ';
          tmp := tmp + Capitalize(s, true);
        end;
      end;
    cmLower:
      tmp := aInput.ToLower;
    cmUpper:
      tmp := aInput.toUpper;
    cmInvert:
      begin
        tmp := '';
        for ch in aInput do
        begin
          tmp := tmp + Invert(ch);
        end;
      end;
    cmFirstLetter:
      tmp := aInput.Substring(0, 1).toUpper + aInput.Substring(1).ToLower;
  end;

  result := '';

  if FIgnoreText = '' then
    result := tmp
  else
  begin
    for s in tmp.Split([' '], TStringSplitOptions.None) do
    begin
      if result <> '' then
        result := result + ' ';
      result := result + CheckIgnore(s);
    end;
  end;
end;

function TCase.ToString: string;
begin
  case FMode of
    cmEveryWord:
      ;
    cmPreserve:
      ;
    cmLower:
      ;
    cmUpper:
      ;
    cmInvert:
      ;
    cmFirstLetter:
      ;
  end;
  result := 'Case';
end;

{ TSerialize }

procedure TSerialize.Next;
begin
  FCurrent := FCurrent + FIndexStep;
end;

function TSerialize.Process(aInput: string): string;
var
  s: string;
begin
  if FPad then
  begin
    s := Format('%%.%dd', [FPadLength]);
  end
  else
  begin
    s := '%d';
  end;
  s := Format(s, [FCurrent]);
  case FPosition of
    spPrefix:
      begin
        result := s + aInput;
      end;
    spSuffix:
      begin
        result := aInput + s;
      end;
    spExact:
      begin
        result := aInput.Insert(FPositionIndex, s);
      end;
  end;
  Next;
end;

procedure TSerialize.Start;
begin
  FCurrent := FIndexStart;
end;

function TSerialize.ToString: string;
begin
  result := 'Serialize';
end;

{ TCleanUp }

function TCleanUp.DoFixSpaces(aInput: string): string;
var
  parts: TArray<string>;
  i: integer;
begin
  parts := aInput.Split([' '], TStringSplitOptions.ExcludeEmpty);
  result := '';
  for i := 0 to high(parts) do
  begin
    if result <> '' then
      result := result + ' ';
    result := result + parts[i];
  end;
end;

function TCleanUp.DoStripBraces(const aInput: string): string;
begin
  result := TRegEx.Replace(aInput, '\(.*?\)', '');
end;

function TCleanUp.DoStripCurlyBraces(const aInput: string): string;
begin
  result := TRegEx.Replace(aInput, '\{.*?\}', '');
end;

function TCleanUp.DoStripSquareBrackets(const aInput: string): string;
begin
  result := TRegEx.Replace(aInput, '\[.*?\]', '');
end;

function TCleanUp.Process(aInput: string): string;
begin
  if FStripBraces then
    aInput := DoStripBraces(aInput);
  if FStripSquareBrackets then
    aInput := DoStripSquareBrackets(aInput);
  if FStripCurlyBraces then
    aInput := DoStripCurlyBraces(aInput);
  if FReplaceDots then
    aInput := aInput.Replace('.', ' ', [rfReplaceAll]);
  if FReplaceComma then
    aInput := aInput.Replace(',', ' ', [rfReplaceAll]);
  if FReplaceUnderscore then
    aInput := aInput.Replace('_', ' ', [rfReplaceAll]);
  if FReplacePlus then
    aInput := aInput.Replace('+', ' ', [rfReplaceAll]);
  if FReplaceMinus then
    aInput := aInput.Replace('-', ' ', [rfReplaceAll]);
  if FFixSpaces then
    aInput := DoFixSpaces(aInput);
  result := aInput;
end;

function TCleanUp.ToString: string;
var
  strip, Replace: string;
begin
  result := 'CleanUp ';
  strip := '';
  Replace := '';
  if FStripBraces then
    strip := strip + '()';
  if FStripSquareBrackets then
    strip := strip + '[]';
  if FStripCurlyBraces then
    strip := strip + '{}';
  if strip <> '' then
    result := result + 'Strip ' + strip + ' ';
  if FReplaceDots then
    Replace := Replace + '.';
  if FReplaceComma then
    Replace := Replace + ',';
  if FReplaceUnderscore then
    Replace := Replace + '_';
  if FReplacePlus then
    Replace := Replace + '+';
  if FReplaceMinus then
    Replace := Replace + '-';
  if Replace <> '' then
    result := result + 'Replace ' + Replace + ' ';
  if FFixSpaces then
    result := result + 'Fix Spaces';
end;

{ TTextProcessor }

function TTextProcessor.getActive: boolean;
begin
  result := fActive;
end;

function TTextProcessor.getClassName: String;
begin
  result := self.ClassName;
end;

function TTextProcessor.Process(aInput: string): string;
begin
  result := '';
end;

procedure TTextProcessor.setActive(const Value: boolean);
begin
  fActive := Value;
end;

procedure TTextProcessor.Start;
begin

end;

function TTextProcessor.ToString: string;
begin
  result := 'TTextProcessor';
end;

end.
